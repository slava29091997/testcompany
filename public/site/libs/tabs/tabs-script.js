jQuery('.js-tab-item').click(function(e) {
    var a = jQuery(this),
        parent = a.parents('.js-tabs-container'),
        nav = parent.children('.js-tabs-nav').children('li'),
        box = parent.children('.js-tabs-box').children('div');

    if (!a.hasClass('active')) {
        a.addClass('active')
            .siblings().removeClass('active');


        box.eq(a.index()).addClass('active')
            .siblings().removeClass('active');
    }


    e.preventDefault();
});
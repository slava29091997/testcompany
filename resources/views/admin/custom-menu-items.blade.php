@foreach($items as $item)

		<tr>
			<td>{{ $item->id }}</td>
			<td style="text-align: left;">{{ $paddingLeft }} {!! Html::link(route('menus.index',['menus' => $item->id]),$item->title) !!}</td>
			<td>{{ $item->url() }}</td>
			<td>{{ $item->parent }}</td>

			<td>
			{!! Form::open(['url' => route('menus.destroy',['menus'=> $item->id]),'class'=>'form-horizontal','method'=>'POST']) !!}
												    {{ method_field('DELETE') }}
												    {!! Form::button('Удалить', ['class' => 'btn btn-french-5','type'=>'submit']) !!}
												{!! Form::close() !!}

			</td>
		</tr>		
		 @if($item->hasChildren())
		        
		        @include('admin.custom-menu-items', array('items' => $item->children(),'paddingLeft' => $paddingLeft.'--'))

		 @endif

@endforeach
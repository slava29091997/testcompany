@extends('layouts.admin')


@section('content')

	<div id="content-page" class="content group">
		<div class="hentry group">

			{!! Form::open(['url' => (isset($menu->id)) ? route('menus.update',['menus'=>$menu->id]) : route('menus.store'),'class'=>'contact-form','method'=>'POST','enctype'=>'multipart/form-data']) !!}

			<ul>

				<li class="text-field">
					<label for="name-contact-us">
						<span class="label">Заголовок:</span>
						<br />
						<span class="sublabel">Заголовок пункта</span><br />
					</label>
					<div class="input-prepend"><span class="add-on"><i class="icon-user"></i></span>
						{!! Form::text('title',isset($menu->title) ? $menu->title  : old('title'), ['placeholder'=>'Введите название страницы']) !!}
					</div>
				</li>


				<li class="text-field">
					<label for="name-contact-us">
						<span class="label">Родительский пункт меню:</span>
						<br />
						<span class="sublabel">Родитель:</span><br />
					</label>
					<div class="input-prepend">
						{!! Form::select('parent', $menus, isset($menu->parent) ? $menu->parent : null) !!}
					</div>

				</li>
			</ul>



			<br />

			@if(isset($menu->id))
				<input type="hidden" name="_method" value="PUT">

			@endif
			<ul>
				<li class="submit-button">
					{!! Form::button('Сохранить', ['class' => 'btn btn-the-salmon-dance-3','type'=>'submit']) !!}
				</li>
			</ul>





		</div>
	</div>

			{!! Form::close() !!}





@endsection


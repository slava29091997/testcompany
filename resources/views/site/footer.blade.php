<!-- BEGIN footer -->
<div class="footer">


    <!-- BEGIN footer-top -->
    <div class="footer-top">
        <div class="container">
            <div class="footer-top__box">
                <ul class="menu-list footer-top__menu-list">
                    <li>
                        <a href=""><span>О нас</span></a>
                    </li>
                    <li>
                        <a href=""><span>Услуги</span></a>
                    </li>
                    <li>
                        <a href=""><span>Дизайн</span></a>
                    </li>
                    <li>
                        <a href=""><span>Портфолио</span></a>
                    </li>
                    <li>
                        <a href=""><span>Шаблоны</span></a>
                    </li>
                    <li>
                        <a href=""><span>Требования к макетам</span></a>
                    </li>
                    <li>
                        <a href=""><span>Отзывы</span></a>
                    </li>
                    <li>
                        <a href=""><span>Вакансии</span></a>
                    </li>
                    <li>
                        <a href=""><span>Статьи</span></a>
                    </li>
                    <li>
                        <a href=""><span>Доставка</span></a>
                    </li>
                    <li>
                        <a href=""><span>Контакты</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END footer-top -->




    <div class="container">
        <div class="footer__box">


            <div class="footer-logo">
                <a href="#"><img src="img/svg/logo print mastering white.svg" alt=""></a>
            </div>
            <div class="footer-nav">
                <ul class="footer-nav__list">
                    <li>
                        <a href="#"><span>Флаеры</span></a>
                    </li>
                    <li>
                        <a href="#"><span>Визитки</span></a>
                    </li>
                    <li>
                        <a href="#"><span>Листовки</span></a>
                    </li>
                    <li>
                        <a href="#"><span>Буклеты</span></a>
                    </li>
                </ul>
                <ul class="footer-nav__list">
                    <li>
                        <a href="#"><span>Каталоги</span></a>
                    </li>
                    <li>
                        <a href="#"><span>Журналы</span></a>
                    </li>
                    <li>
                        <a href="#"><span>Книги</span></a>
                    </li>
                    <li>
                        <a href="#"><span>Плакаты</span></a>
                    </li>
                </ul>
                <ul class="footer-nav__list">
                    <li>
                        <a href="#"><span>Постеры</span></a>
                    </li>
                    <li>
                        <a href="#"><span>Календари</span></a>
                    </li>
                    <li>
                        <a href="#"><span>Обечайка</span></a>
                    </li>
                </ul>
                <ul class="footer-nav__list">
                    <li>
                        <a href="#"><span>Конверты</span></a>
                    </li>
                    <li>
                        <a href="#"><span>Коробки</span></a>
                    </li>
                </ul>
            </div>
            <div class="footer-contacts">
                <a href="tel:7 499 112 06 93" class="footer-contacts__phone">7 499 112 06 93</a>
                <div class="footer-contacts__info">
                    Талалихина, дом 41, строение 26
                    <span>с 10:00 до 18:00 Пн-Пт</span>
                </div>
                <div class="my-btn-wrap">
                    <a href="#" class="my-btn"><span>Написать отзыв на YELL</span></a>
                </div>
            </div>

        </div>

        <div class="copyright">
            <div class="copyright__text">
                <p>Типография "Альма Пресс" 2010-2017</p>
            </div>

            <div class="soc-links">
                <ul>
                    <li>
                        <a href="#"><img src="img/svg/facebook.svg" alt=""></a>
                    </li>
                    <li>
                        <a href="#"><img src="img/svg/vk.svg" alt=""></a>
                    </li>
                </ul>

            </div>

            <div class="creator">
                <div class="creator__text">
                    <p>Разработка  и поддержка</p>
                </div>
                <div class="creator__img">
                    <a href="#" target="_blank">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 11 11" style="enable-background:new 0 0 11 11;" xml:space="preserve">
<style type="text/css">
    .st0{clip-path:url(#SVGID_2_);enable-background:new    ;}
    .st1{clip-path:url(#SVGID_4_);}
    .st2{clip-path:url(#SVGID_6_);}
</style>
                            <g>
                                <defs>
                                    <path id="SVGID_1_" d="M10.4,0H1.3L0,2.7h10.4V0z M10.5,4H5.3C3.7,4,2.5,4.5,1.6,5.4C0.9,6.1,0.5,6.9,0.2,8c0,0.3-0.1,0.4-0.1,0.7
			c0,0.3-0.1,0.4-0.1,0.7v1.4h2.6V9.3C2.7,8.2,3.1,7.5,4,7c0.2-0.2,0.7-0.3,1.3-0.3h5.2V4z"/>
                                </defs>
                                <clipPath id="SVGID_2_">
                                    <use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
                                </clipPath>
                                <g class="st0">
                                    <g>
                                        <defs>
                                            <rect id="SVGID_3_" x="-5" y="-5" width="21" height="21"/>
                                        </defs>
                                        <clipPath id="SVGID_4_">
                                            <use xlink:href="#SVGID_3_"  style="overflow:visible;"/>
                                        </clipPath>
                                        <g class="st1">
                                            <defs>
                                                <rect id="SVGID_5_" width="11" height="11"/>
                                            </defs>
                                            <clipPath id="SVGID_6_">
                                                <use xlink:href="#SVGID_5_"  style="overflow:visible;"/>
                                            </clipPath>
                                            <rect x="-5" y="-5" class="st2" width="20.5" height="20.7"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
</svg>
                    </a>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- END footer -->



<!--END main-wrapper-->

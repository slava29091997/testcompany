<!-- BEGIN header -->
<div class="header">
    <div class="container">
        <div class="header__box">
            <div class="header-nav clearfix">
                <ul class="menu-list">
                    <li>
                        <a href=""><span>О нас</span></a>
                    </li>
                    <li>
                        <a href=""><span>Услуги</span></a>
                    </li>
                    <li>
                        <a href=""><span>Дизайн</span></a>
                    </li>
                    <li>
                        <a href=""><span>Портфолио</span></a>
                    </li>
                    <li>
                        <a href=""><span>Шаблоны</span></a>
                    </li>
                    <li>
                        <a href=""><span>Требования к макетам</span></a>
                    </li>
                    <li>
                        <a href=""><span>Отзывы</span></a>
                    </li>
                    <li>
                        <a href=""><span>Вакансии</span></a>
                    </li>
                    <li>
                        <a href=""><span>Статьи</span></a>
                    </li>
                    <li>
                        <a href=""><span>Доставка</span></a>
                    </li>
                    <li>
                        <a href=""><span>Контакты</span></a>
                    </li>
                </ul>
                <div class="user-nav">
                    <a href=""><span>Войти</span></a>
                    <a href=""><span>Регистрация</span></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END header -->


<!-- BEGIN header-info -->
<div class="header-info">
    <div class="container">
        <div class="header-info__box">
            <a href="" class="header-logo">
                <img src="img/svg/logo print mastering.svg" alt="">
            </a>

            <div class="header-contacts">
                <div class="header-contacts__left">
                    <div class="header-contacts__info">
                        <p>Талалихина, дом 41, строение 26
                            <span>с 10:00 до 18:00 Пн-Пт</span>
                        </p>

                    </div>
                </div>
                <div class="header-contacts__right">


                    <a href="tel:7 499 112 06 93" class="phone-number">7 499 112 06 93</a>

                    <div class="my-btn-wrap header-contacts__my-btn-wrap">
                        <a href="" class="my-btn"><span>Расчет заказа</span></a>
                    </div>

                </div>
            </div>

        </div>

    </div>
</div>
<!-- END header-info -->




<!-- BEGIN category-nav -->






@if($menu)
    <div class="category-nav">
        <div class="container">
            <div class="category-nav__content">
                <ul class="category-nav-list menu--prospero clearfix">
                    @include('site.customMenuItems',['items'=>$menu->roots()])
                </ul>
            </div>
        </div>
    </div>

@endif
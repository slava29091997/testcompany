
    <!-- BEGIN breadcrumbs -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="breadcrumbs__content">

                <ul>
                    <li>
                        <a href="#">Главная</a>
                    </li>
                    <li>
                        <a href="#">Услуги</a>
                    </li>
                    <li>
                        <p>Буклет</p>
                    </li>
                </ul>

            </div>
        </div>
    </div>
    <!-- END breadcrumbs -->







    <!-- BEGIN calculator -->
    <div class="calculator">
        <div class="container">
            <div class="calculator__content clearfix">

                <div class="calculator-box wow up-animation" data-wow-delay=".1s" data-wow-duration="1s">

                    <div class="tabs js-tabs-container">
                        <ul class="tabs-nav calculator-nav-tab js-tabs-nav clearfix">

                            <li class=" js-tab-item active">
                                <a class="">Калькулятор</a>
                            </li>
                            <li class="js-tab-item">
                                <a class="">Прайс-лист</a>
                            </li>

                        </ul>
                        <div class="tabs-box calculator__tabs-box js-tabs-box">

                            <div class="calc-content-box active">

                                <div class="calc-type">
                                    <a href="" class="active">Обычный калькулятор</a>
                                    <a href="">Профессиональный калькулятор</a>
                                </div>








                                <div class="calc">
                                    <div class="calc__item">
                                        <div class="calc-select-grid calc-select-grid_2">
                                            <div class="calc-select-grid__item">
                                                <div >
                                                    <div class="custom-select__title">
                                                        Плотность бумаги
                                                    </div>
                                                    <select id="density" class="bs-donebutton" name="a1">
                                                        <option value="5">130г/м</option>
                                                        <option value="6">150г/м</option>
                                                        <option value="7">170г/м</option>
                                                        <option value="8">190г/м</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="calc-select-grid__item">
                                                <div >
                                                    <div class="custom-select__title">
                                                        Формат листовки
                                                    </div>
                                                    <select id="leaflets" class="bs-donebutton">
                                                        <option value="5">A6, 105x148мм</option>
                                                        <option value="6">A4, 30x02мм</option>
                                                        <option value="7">A3, 150x148мм</option>
                                                        <option value="8">A1, 300x148мм</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="calc-select-grid__item">
                                                <div >
                                                    <div class="custom-select__title">
                                                        Тираж
                                                    </div>
                                                    <select class="bs-donebutton" id="circulation">
                                                        <option value="5">500 шт.</option>
                                                        <option value="10">1000 шт.</option>
                                                        <option value="15">1500 шт.</option>
                                                        <option value="20">2000+ шт.</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="calc-select-grid__item">
                                                <div >
                                                    <div class="custom-select__title">
                                                        Полноцветная печать
                                                    </div>
                                                    <select class="bs-donebutton" id="printing">
                                                        <option value="10">С двух сторон</option>
                                                        <option value="5">С одной стороны</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="calc__item">
                                        <div class="custom-checkbox custom-checkbox_big custom-checkbox_big--no-pad">
                                            <label class="custom-checkbox__label">
                                                <input type="checkbox" class="custom-checkbox__input" style="display: none;">
                                                <span class="custom-checkbox__custom-input">

                                                </span>
                                                <span class="custom-checkbox__text" id="design">
                                                    Разработка дизайна
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="calc__item">
                                        <div class="custom-checkbox custom-checkbox_big custom-checkbox_big--no-pad">
                                            <label class="custom-checkbox__label">
                                                <input type="checkbox" class="custom-checkbox__input" style="display: none;">
                                                <span class="custom-checkbox__custom-input">

                                                </span>
                                                <span class="custom-checkbox__text">
                                                   Дополнительная послепечатная обработка
                                                </span>
                                            </label>
                                        </div>
                                        <div class="calc__btn calc__btn_big-pad">
                                            <a href="#" id="calculate" class="my-btn" name="s" ><span >Рассчитать</span></a>
                                        </div>
                                    </div>
                                </div>

                                <script>
                                    document.getElementById('calculate').onclick = function() {
                                        var density = document.getElementById('density').options[document.getElementById('density').selectedIndex].text;
                                        document.getElementById('density-result').innerHTML = density;

                                        var leaflets = document.getElementById('leaflets').options[document.getElementById('leaflets').selectedIndex].text;
                                        document.getElementById('leaflets-result').innerHTML = leaflets;

                                        var circulation = document.getElementById('circulation').options[document.getElementById('circulation').selectedIndex].text;
                                        document.getElementById('circulation-result').innerHTML = circulation;

                                        var printing = document.getElementById('printing').options[document.getElementById('printing').selectedIndex].text;
                                        document.getElementById('printing-result').innerHTML = printing;

                                        var densityInt = Number(document.getElementById('density').value);
                                        var leafletsInt = Number(document.getElementById('leaflets').value);
                                        var circulationInt = Number(document.getElementById('circulation').value);
                                        var printingInt = Number(document.getElementById('printing').value);


                                        var weight = densityInt +leafletsInt+circulationInt+printingInt;

                                        var amount = (densityInt +leafletsInt+circulationInt+printingInt)*20;


                                        document.getElementById('weight-result').innerHTML = weight+" кг.";
                                        document.getElementById('amount-result').innerHTML = amount+" руб.";
                                    };
                                </script>



                            </div>

                            <div class="price-list-box">
                                <div class="price-table">
                                    <table  cellspacing="0" cellpadding="0">
                                        <thead>
                                        <tr>
                                            <td>Формат, мм / тираж, шт.</td>
                                            <td>200 шт</td>
                                            <td>500 шт</td>
                                            <td>1000 шт</td>
                                            <td>2000 шт</td>
                                            <td>5000 шт</td>
                                            <td>10000 шт</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>90х50 мм. 300 гр.<br>
                                                Полноцветная с 2 сторон</td>
                                            <td>1590&#8381;</td>
                                            <td>1793&#8381;</td>
                                            <td>1816&#8381;</td>
                                            <td>2260&#8381;</td>
                                            <td>5520&#8381;</td>
                                            <td>9200&#8381;</td>
                                        </tr>
                                        <tr>
                                            <td>90х50 мм.300 гр<br>
                                                Полноцветная с 1 стороны<br>
                                                (оборот белый)</td>
                                            <td>1590&#8381;</td>
                                            <td>1793&#8381;</td>
                                            <td>1816&#8381;</td>
                                            <td>2260&#8381;</td>
                                            <td>5520&#8381;</td>
                                            <td>9200&#8381;</td>
                                        </tr>
                                        <tr>
                                            <td>Скругление углов</td>
                                            <td>1590&#8381;</td>
                                            <td>1793&#8381;</td>
                                            <td>1816&#8381;</td>
                                            <td>2260&#8381;</td>
                                            <td>5520&#8381;</td>
                                            <td>9200&#8381;</td>
                                        </tr>
                                        <tr>
                                            <td>Ламинация визитки с 2 сторон</td>
                                            <td>1590&#8381;</td>
                                            <td>1793&#8381;</td>
                                            <td>1816&#8381;</td>
                                            <td>2260&#8381;</td>
                                            <td>5520&#8381;</td>
                                            <td>9200&#8381;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>



                </div>

                <div class="check-side wow up-animation" data-wow-delay=".2s" data-wow-duration="1s">
                    <div class="check-side__content">
                        <div class="check-side__title">
                            <p>Стоимость заказа</p>
                        </div>
                        <div class="check-side__item">
                            <div class="check-side__value clearfix">
                                <span class="description">Плотность бумаги</span>
                                <span class="check-value" id="density-result">130 г/м</span>
                            </div>

                            <div class="check-side__value clearfix">
                                <span class="description">Формат листовки</span>
                                <span class="check-value" id="leaflets-result">А6, 105х148мм</span>
                            </div>
                            <div class="check-side__value clearfix">
                                <span class="description">Тираж</span>
                                <span class="check-value" id="circulation-result">500 шт.</span>
                            </div>
                            <div class="check-side__value clearfix">
                                <span class="description">Полноцветная печать </span>
                                <span class="check-value" id="printing-result">С двух сторон</span>
                            </div>
                        </div>
                        <div class="check-side__item">
                            <div class="check-side__value clearfix">
                                <span class="description"> Вес тиража</span>
                                <span class="check-value" id="weight-result">5 кг</span>
                            </div>
                        </div>
                        <div class="check-side__item">
                            <div class="check-side__value clearfix">
                                <span class="description">Дизайн визитки </span>
                                <span class="check-value">500-1500 руб</span>
                            </div>
                        </div>
                        <div class="check-side__item check-side__item_price">
                            <div class="check-side__value clearfix">
                                <span class="description">Сумма заказа:   </span>
                                <span class="check-value"><span class="check-price" id="amount-result">4000 руб.</span></span>
                            </div>
                        </div>
                        <div class="check-side__btn">
                            <a href="#" class="my-btn"> <span>Заказать</span></a>
                        </div>
                    </div>

                    <div class="tech-req">
                        <a href="#"><span>Технические требования</span></a>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- END calculator -->













@foreach($items as $item)
    @if(!$item->hasChildren())
        <li class="<?= (!isset($i)) ? 'category-nav-list__item category-parent' : ''?>">
            <a href="#" class="<?=(!isset($i)) ? 'category-nav-list__link' : ''?> ">
                {{$item->title}}
            </a>
    @elseif($item->hasChildren())
        <li class="category-nav-list__item category-parent">
            <a href="#" class="category-nav-list__link ">
                {{$item->title}}
            </a>
            <div class="category-sub-menu">
                <div class="category-sub-menu__box">
                    <div class="container">
                        <ul>
                            <li>
                                @include('site.customMenuItems',['items'=>$item->children(), 'i' => 1])
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>

    @endif
@endforeach



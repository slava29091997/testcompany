@extends('layouts.site')

@section('navigation')
    @include('site.navigation')
@endsection

@section('content')
    @include('site.content')
@endsection

@section('footer')
    @include('site.footer')
@endsection
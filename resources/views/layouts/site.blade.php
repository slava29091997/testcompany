<!doctype html>
<html>
<head>
    <meta charset="utf-8">

    <title>Календари</title>
    <meta name="description" content="">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="site/img/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="site/styles/css/fonts.css">
    <link rel="stylesheet" href="site/styles/css/component.css">
    <link rel="stylesheet" href="site/styles/css/main.css">

    <link rel="stylesheet" href="site/css/bootstrap-select.css">


    <script type="text/javascript" src="site/assets/js/bootstrap-select.js"></script>



</head>
<body>
<div class="main-wrapper">

    @yield('navigation')


    @yield('content')


    @yield('footer')
</div>
<!-- Прячем контент -->
<div class="hidden">


</div><!-- Прячем контент -->


<script src="libs/jquery/jquery-2.2.4.min.js"></script>
<script src="libs/fancybox/jquery.fancybox.js"></script>
<script src="libs/slick/slick.js"></script>
<script src="libs/form-styler/form-styler.js"></script>
<script src="libs/animate/wow.min.js"></script>
<script src="libs/tabs/tabs-script.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/anime.min.js"></script>
<script src="js/main.js"></script>
<script>
    (function() {
        var tiltSettings = [
            {},
            {
                movement: {
                    imgWrapper : {
                        translation : {x: 10, y: 10, z: 30},
                        rotation : {x: 0, y: -10, z: 0},
                        reverseAnimation : {duration : 200, easing : 'easeOutQuad'}
                    },
                    lines : {
                        translation : {x: 10, y: 10, z: [0,70]},
                        rotation : {x: 0, y: 0, z: -2},
                        reverseAnimation : {duration : 2000, easing : 'easeOutExpo'}
                    },
                    caption : {
                        rotation : {x: 0, y: 0, z: 2},
                        reverseAnimation : {duration : 200, easing : 'easeOutQuad'}
                    },
                    overlay : {
                        translation : {x: 10, y: -10, z: 0},
                        rotation : {x: 0, y: 0, z: 2},
                        reverseAnimation : {duration : 2000, easing : 'easeOutExpo'}
                    },
                    shine : {
                        translation : {x: 100, y: 100, z: 0},
                        reverseAnimation : {duration : 200, easing : 'easeOutQuad'}
                    }
                }
            },
            {
                movement: {
                    imgWrapper : {
                        rotation : {x: -5, y: 10, z: 0},
                        reverseAnimation : {duration : 900, easing : 'easeOutCubic'}
                    },
                    caption : {
                        translation : {x: 30, y: 30, z: [0,40]},
                        rotation : {x: [0,15], y: 0, z: 0},
                        reverseAnimation : {duration : 1200, easing : 'easeOutExpo'}
                    },
                    overlay : {
                        translation : {x: 10, y: 10, z: [0,20]},
                        reverseAnimation : {duration : 1000, easing : 'easeOutExpo'}
                    },
                    shine : {
                        translation : {x: 100, y: 100, z: 0},
                        reverseAnimation : {duration : 900, easing : 'easeOutCubic'}
                    }
                }
            },
            {
                movement: {
                    imgWrapper : {
                        rotation : {x: -5, y: 10, z: 0},
                        reverseAnimation : {duration : 50, easing : 'easeOutQuad'}
                    },
                    caption : {
                        translation : {x: 20, y: 20, z: 0},
                        reverseAnimation : {duration : 200, easing : 'easeOutQuad'}
                    },
                    overlay : {
                        translation : {x: 5, y: -5, z: 0},
                        rotation : {x: 0, y: 0, z: 6},
                        reverseAnimation : {duration : 1000, easing : 'easeOutQuad'}
                    },
                    shine : {
                        translation : {x: 50, y: 50, z: 0},
                        reverseAnimation : {duration : 50, easing : 'easeOutQuad'}
                    }
                }
            },
            {
                movement: {
                    imgWrapper : {
                        translation : {x: 0, y: -8, z: 0},
                        rotation : {x: 3, y: 3, z: 0},
                        reverseAnimation : {duration : 1200, easing : 'easeOutExpo'}
                    },
                    lines : {
                        translation : {x: 15, y: 15, z: [0,15]},
                        reverseAnimation : {duration : 1200, easing : 'easeOutExpo'}
                    },
                    overlay : {
                        translation : {x: 0, y: 8, z: 0},
                        reverseAnimation : {duration : 600, easing : 'easeOutExpo'}
                    },
                    caption : {
                        translation : {x: 10, y: -15, z: 0},
                        reverseAnimation : {duration : 900, easing : 'easeOutExpo'}
                    },
                    shine : {
                        translation : {x: 50, y: 50, z: 0},
                        reverseAnimation : {duration : 1200, easing : 'easeOutExpo'}
                    }
                }
            },
            {
                movement: {
                    lines : {
                        translation : {x: -5, y: 5, z: 0},
                        reverseAnimation : {duration : 1000, easing : 'easeOutExpo'}
                    },
                    caption : {
                        translation : {x: 15, y: 15, z: 0},
                        rotation : {x: 0, y: 0, z: 3},
                        reverseAnimation : {duration : 1500, easing : 'easeOutElastic', elasticity : 700}
                    },
                    overlay : {
                        translation : {x: 15, y: -15, z: 0},
                        reverseAnimation : {duration : 500,easing : 'easeOutExpo'}
                    },
                    shine : {
                        translation : {x: 50, y: 50, z: 0},
                        reverseAnimation : {duration : 500, easing : 'easeOutExpo'}
                    }
                }
            },
            {
                movement: {
                    imgWrapper : {
                        translation : {x: 5, y: 5, z: 0},
                        reverseAnimation : {duration : 800, easing : 'easeOutQuart'}
                    },
                    caption : {
                        translation : {x: 10, y: 10, z: [0,50]},
                        reverseAnimation : {duration : 1000, easing : 'easeOutQuart'}
                    },
                    shine : {
                        translation : {x: 50, y: 50, z: 0},
                        reverseAnimation : {duration : 800, easing : 'easeOutQuart'}
                    }
                }
            },
            {
                movement: {
                    lines : {
                        translation : {x: 40, y: 40, z: 0},
                        reverseAnimation : {duration : 1500, easing : 'easeOutElastic'}
                    },
                    caption : {
                        translation : {x: 20, y: 20, z: 0},
                        rotation : {x: 0, y: 0, z: -5},
                        reverseAnimation : {duration : 1000, easing : 'easeOutExpo'}
                    },
                    overlay : {
                        translation : {x: -30, y: -30, z: 0},
                        rotation : {x: 0, y: 0, z: 3},
                        reverseAnimation : {duration : 750, easing : 'easeOutExpo'}
                    },
                    shine : {
                        translation : {x: 100, y: 100, z: 0},
                        reverseAnimation : {duration : 750, easing : 'easeOutExpo'}
                    }
                }
            }];

        function init() {
            var idx = 0;
            [].slice.call(document.querySelectorAll('a.tilter')).forEach(function(el, pos) {
                idx = pos%2 === 0 ? idx+1 : idx;
                new TiltFx(el, tiltSettings[idx-1]);
            });
        }

        // Preload all images.
        imagesLoaded(document.querySelector('main'), function() {
            document.body.classList.remove('loading');
            init();
        });

        // REMOVE THIS!
        // For Demo purposes only. Prevent the click event.
        [].slice.call(document.querySelectorAll('a[href="#"]')).forEach(function(el) {
            el.addEventListener('click', function(ev) { ev.preventDefault(); });
        });

        var pater = document.querySelector('.pater'),
            paterSVG = pater.querySelector('.pater__svg'),
            pathEl = paterSVG.querySelector('path'),
            paths = {default: pathEl.getAttribute('d'), active: paterSVG.getAttribute('data-path-hover')};

        pater.addEventListener('mouseenter', function() {
            anime.remove(pathEl);
            anime({
                targets: pathEl,
                d: paths.active,
                duration: 400,
                easing: 'easeOutQuad'
            });
        });

        pater.addEventListener('mouseleave', function() {
            anime.remove(pathEl);
            anime({
                targets: pathEl,
                d: paths.default,
                duration: 400,
                easing: 'easeOutExpo'
            });
        });
    })();
</script>



<script src="js/custom.js"></script>

</body>
</html>



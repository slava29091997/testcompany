<?php

namespace App\Repositories;

use App\Menu;
use Gate;

class MenusRepository extends Repository
{

    public function __construct(Menu $menu)
    {

        $this->model = $menu;

    }

    public function addMenu($request)
    {


        $data = $request->only('type','title','parent');

        if (empty($data)){
            return ['error'=>'Нет данных'];
        }

        //dd($request->all());
        $data['path']="#";

        if ($this->model->fill($data)->save()){
            return ['status'=>'Ссылка добавлена'];
        }
    }


    public function updateMenu($request,$menu)
    {


        $data = $request->only('type','title','parent');

        if (empty($data)){
            return ['error'=>'Нет данных'];
        }

        //dd($request->all());




        if ($menu->fill($data)->update()){
            return ['status'=>'Ссылка обновлена'];
        }
    }

    public function deleteMenu($menu){

        if ($menu->delete()){
            return ['status'=>'Ссылка удалена'];
        }
    }



}

?>
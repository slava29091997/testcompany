<?php

namespace App\Http\Controllers\Admin;



use App\Repositories\MenusRepository;
use Illuminate\Http\Request;

use Menu;

class MenusController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $m_rep;

    public function __construct(MenusRepository $m_rep)
    {
        parent::__construct();

        $this->m_rep = $m_rep;

    }


    public function index()
    {

        $menu = $this->getMenus();

        return view('admin.menu')->with('menus',$menu);
    }

    public function getMenus()
    {
       $menu = $this->m_rep->get();

       if ($menu->isEmpty()){
            return FALSE;
       }

       return Menu::make('rorMenuPart',function ($m) use ($menu){
           foreach ($menu as $item){
               if ($item->parent == 0){
                   $m->add($item->title,$item->path)->id($item->id);
               }
               else{
                   if ($m->find($item->parent)){
                       $m->find($item->parent)->add($item->title,$item->path)->id($item->id);
                   }
               }
           }
       });

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this ->title = 'Новый пункт меню';

        $tmp = $this->getMenus()->roots();

        $menus = $tmp->reduce(function ($returnMenus,$menu){

            $returnMenus[$menu->id] = $menu->title;
            return $returnMenus;
        },['0'=>'Родительський пункт меню']);


        //$this->content = view(env('THEME').'.admin.menus_create_content')->with(['menus'=>$menus,'categories'=>$list,'articles'=>$articles,'filters' => $filters,'portfolios' => $portfolios])->render();

        return view('admin.menus_create_content')->with(['menus'=>$menus]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $result = $this->m_rep->addMenu($request);

        if(is_array($result) && !empty($result['error'])) {
            return back()->with($result);
        }

        return redirect('admin/menus')->with($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit() //RouteServiceProvider
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Menu $menu)
    {

        $result = $this->m_rep->deleteMenu($menu);

        if(is_array($result) && !empty($result['error'])) {
            return back()->with($result);
        }

        return redirect('admin/menus')->with($result);

    }
}

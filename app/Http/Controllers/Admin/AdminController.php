<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;

use Corp\Http\Requests;
use Corp\Http\Controllers\Controller;

use Auth;
use Menu;



class AdminController extends \App\Http\Controllers\Controller
{
    //

    protected $p_rep;

    protected $a_rep;

    protected $user;

    protected $template;

    protected $content = FALSE;

    protected $title;

    protected $vars;

    public function __construct() {

		$this->user = Auth::user();


	/*	if(!$this->user) {
			abort(403);
		}*/
	}

	public function renderOutput() {


	}

	public function getMenu() {
		return Menu::make('adminMenu', function($menu) {

            $menu->add('Статьи','admin/articless');

           // $menu->add('Портфолио','route');
            $menu->add('Меню','admin/menus');
           // $menu->add('Пользователи','route');
            $menu->add('Привилегии','admin/permissions');

			//$menu->add('Статьи',array('route' => 'admin.articles.index'));

//			$menu->add('Портфолио',array('route' => 'admin.articles.index'));
//			$menu->add('Меню',array('route'  => 'admin.articles.index'));
//			$menu->add('Пользователи',array('route' => 'admin.articles.index'));
//			$menu->add('Привилегии',array('route' => 'admin.articles.index'));


		});
	}
	
	
}

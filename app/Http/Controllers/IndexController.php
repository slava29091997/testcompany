<?php

namespace App\Http\Controllers;


use App\Menu;
use App\Repositories\MenusRepository;
use App\Http\Controllers\SiteController;
use Illuminate\Http\Request;



use DB;

class IndexController extends SiteController
{
    //

    public function __construct(){

        parent::__construct( new \App\Repositories\MenusRepository(new \App\Menu));

    }



    public function execute(Request $request){


        return $this->renderOutput();
    }



}
